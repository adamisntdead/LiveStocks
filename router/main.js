const dataController = require('../controllers/data');
const headerController = require('../controllers/headers');

module.exports = function (app) {
    app.get('/raw/:key', getRaw);
    app.use(headerController);
    app.get('/', index);
};

function getRaw (req, res) {
    dataController.getStockData(req.params.key, function (err, result) {
        if (err) {
            console.log(err);
        }
        res.jsonp(result);
    });
}

function index (req, res) {
    res.render('main');
}