var socket = io(window.location.hostname);
  socket.on('stockKeys', function (data) {
    console.log(data.keys);
    $('.stock-info').empty();
    draw(data.keys)
    for (var i = 0; i < data.keys.length; i++) {
      drawInfo(data.keys[i]);
    }
    drawNew();
  });

function draw(keys) {
    var seriesOptions = [],
        seriesCounter = 0,
        names = keys;
    $.each(names, function (i, name) {
        $.getJSON('raw/' + name, function (data) {
            seriesOptions[i] = {
                name: name,
                data: data.result
            };
            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;
            if (seriesCounter === names.length) {
                createChart(seriesOptions);
            }
        });
    });
}

function createChart(seriesOptions) {
    $('#container').highcharts('StockChart', {
        rangeSelector: {
            selected: 4
        },
        yAxis: {
            labels: {
                formatter: function () {
                    return (this.value > 0 ? ' + ' : '') + this.value + '%';
                }
            },
            plotLines: [{
                value: 0,
                width: 2,
                color: 'silver'
            }]
        },
        plotOptions: {
            series: {
                compare: 'percent'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
            valueDecimals: 2
        },
        series: seriesOptions
    });
}

function drawInfo(key) {
  $('.stock-info').append('<div class="col-xs-6 ' + key + '"> \
  <div class="alert animated fadeIn alert-success"> \
    <a type="button" href="javascript:deleteStock(\'' + key + '\')" class="close">×</a> \
    <h3> <strong class="text-center">' + key + '</strong></h3> \
  </div> \
</div>');
}

function drawNew() {
  $('.stock-info').append('<div class="col-xs-6"><a href="#addModal" data-toggle="modal" class="alert-link"><div class="alert alert-info"><h3>Add</h3></div></a></div>');
}

function deleteStock(key) {
  socket.emit('delete', {
      key: key
  });
}

function submitStock() {
  var key = $('#stockInput').val();
  console.log('Submitting: ' + key)
  socket.emit('new', {
      key: key
  });
  $('#addModal').modal('hide');
}
