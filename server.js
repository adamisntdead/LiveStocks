// Require Node Modules
const express = require('express');
const logger = require('morgan');
const compression = require('compression');
const path = require('path');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const async = require('async');
const socketio = require('socket.io');
const http = require('http');

// Add .env
require('dotenv').config({
    silent: true
});

// Setup App
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(methodOverride('_method'));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
require('./router/main.js')(app);

// Variables and socketio Stuff
var stockKeys = ['MSFT', 'IBM', 'AAPL'];
io.on('connection', function(socket) {
    socket.emit('stockKeys', {
        keys: stockKeys
    });
    socket.on('new', function(data) {
        stockKeys.push(data.key.toUpperCase());
        socket.emit('stockKeys', {
            keys: stockKeys
        });
        socket.broadcast.emit('stockKeys', {
            keys: stockKeys
        });
    });
    socket.on('delete', function(data) {
        console.log('Got Delete Request: ' + data)
        var index = stockKeys.indexOf(data.key.toUpperCase());
        if (index > -1) {
            stockKeys.splice(index, 1);
        }
        socket.emit('stockKeys', {
            keys: stockKeys
        });
        socket.broadcast.emit('stockKeys', {
            keys: stockKeys
        });
    });
});


// Production error handler
if (app.get('env') === 'production') {
    app.use(function(err, req, res, next) {
        console.error(err.stack);
        res.sendStatus(err.status || 500);
    });
}

server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
