var Quandl = require("quandl");
var quandl = new Quandl({
    auth_token: 'xm8o5xU_9RLNciNVtxmq',
    api_version: 3
});

exports.getStockData = function(key, callback) {
    quandl.dataset({
        source: "WIKI",
        table: key
    }, {
        order: "asc",
        exclude_column_names: true,
        // Notice the YYYY-MM-DD format
        start_date: "2013-01-30",
        end_date: "2016-01-30",
        column_index: 4
    }, function(err, response) {
        var done = {};
        var parsed = JSON.parse(response);
        if (parsed.quandl_error) {
            callback(err, []);
        } else {
            var result = [];
            for (var i = 0; i < parsed.dataset.data.length; i++) {
                var formatedDate = parsed.dataset.data[i][0].split('-').join('.');
                var timestamp = parseInt((new Date(formatedDate).getTime()).toFixed(0));

                result.push([timestamp, parsed.dataset.data[i][1]]);
            }
            done.result = result;

            callback(err, done);
        }
    });
};
